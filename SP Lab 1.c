#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define Max_Employee 1024
#define Max_Name 64
#define Max_Char 1000

static struct Employee { //construct a structure for employee to store information
    int employee_id;
    char first_name[Max_Name];
    char last_name[Max_Name];
    int salary;
} list[Max_Employee];

int list_num(struct Employee list[]) {
    int count = 0;
    for (int i = 0; list[i].employee_id != 0; i++) {
        count += 1;
    }
    return count;
}

void print_menu(){ //function to print out the main menu
    printf("Employee Database Menu:\n");
    printf("------------------------------\n");
    printf("1. Print the Database\n");
    printf("2. Look Up by Employee ID\n");
    printf("3. Look Up by Last Name\n");
    printf("4. Add an Employee\n");
    printf("5. Exit\n");
    printf("------------------------------\n");
    printf("Enter your choice: ");
}

void print_database(FILE* filename){ //function to print the employee database
    printf("\nEmployee ID    First Name    Last Name      Salary\n");
    printf("-------------------------------------------------------\n");
    int i = 0;
    int num_of_employee = list_num(list);
    for (i; i < num_of_employee - 1; i++) { //sort the list by employee ID
        for (int j = 0; j < num_of_employee - i - 1; j++) {
            if (list[j].employee_id > list[j+1].employee_id){
                struct Employee temp;
                temp = list[j];
                list[j] = list[j+1];
                list[j+1] = temp;
            }
        }
    }
    i = 0;
    while (list[i].employee_id != 0) { //print out the employee database
        printf("  %-13d %-13s %-14s%6d\n", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
        i++;
    }
    printf("-------------------------------------------------------\n");
    printf("Number of Employees: %d\n\n", num_of_employee); //print out the number of employees in the database
}

void search_by_id(struct Employee list[], int l, int r, int x) { //function to search by ID using binary search
    int found = 0;
    while (l <= r) {
        int mid = l + (r - l) / 2;
        if (list[mid].employee_id == x) {
            printf("\nEmployee ID    First Name    Last Name      Salary\n");
            printf("-------------------------------------------------------\n");
            printf("  %-13d %-13s %-14s%6d\n", list[mid].employee_id, list[mid].first_name, list[mid].last_name, list[mid].salary);
            printf("-------------------------------------------------------\n\n");
            found = 1;
            break;
        }
        if (list[mid].employee_id < x) {
            l = mid + 1;
            found = 0;
        }
        else {
            r = mid - 1;
            found = 0;
        }
    }
    if (found == 0) {
        printf("\nEmployee with the ID %d cannot be found in the database.\n\n", x);
    }
}

void search_by_last_name(char x[Max_Char]) { //function to search by last name
    int found = 0;
    int match = 0;
    struct Employee match_list[Max_Employee]; //create a new list to store the employees who match the search criteria
    for (int i = 0; i < Max_Employee; i++) {
        if (strcasecmp(x, list[i].last_name) == 0) { //check whether two strings are case-insensitively equal
            match_list[match] = list[i];
            match++;
            found = 1;
        } else if (found != 1) {
            found = 0;
        }
    }
    if (match == 0) { //print out the "not found" message
        printf("\nEmployee with the last name %s is not found in the database.\n\n", x);
    } else { //print out the employees with given last name
        printf("\nEmployee ID    First Name    Last Name      Salary\n");
        printf("-------------------------------------------------------\n");
        for (int j = 0; j < match; j++) {
            printf("  %-13d %-13s %-14s%6d\n", match_list[j].employee_id, match_list[j].first_name, match_list[j].last_name, match_list[j].salary);
        }
        printf("-------------------------------------------------------\n\n");
    }
}

void add_employee(FILE* filename) { //function to add an employee
    char fname[Max_Name], lname[Max_Name], choice[Max_Char]; //define three strings to read user's input
    int salary;
    int i = 0;
    int check = 0;
    int num_of_employee = list_num(list);
    printf("\nEnter the first name of the employee: ");
    getchar();
    scanf("%[^\n]", fname);
    do { // check whether the field contains extra spaces
        i = 0;
        check = 0;
        while(fname[i] != '\0') {
            if (isalpha(fname[i]) == 0) {
                check = 1;
                printf("\nPlease only enter the name with no extra spaces or middle initials. Please re-enter: ");
                getchar();
                scanf("%[^\n]", fname);
                break;
            }
            i++;
        }
    } while (check == 1);
    i = 0;
    check = 0;
    printf("\nEnter the last name of the employee: ");
    getchar();
    scanf("%[^\n]", lname);
    do { //check whether the field contains extra spaces
        i = 0;
        check = 0;
        while(lname[i] != '\0') {
            if (isalpha(lname[i]) == 0) {
                check = 1;
                printf("\nPlease only enter the name with no extra spaces or middle initials. Please re-enter: ");
                getchar();
                scanf("%[^\n]", lname);
                break;
            }
            i++;
        }
    } while (check == 1);
    printf("\nEnter the salary for the employee (30000 to 150000): ");
    scanf("%d", &salary);
    while (salary < 30000 || salary > 150000) {
        printf("\nEnter an integer between 30000 and 150000. Please re-enter: ");
        scanf("%d", &salary);
    }
    printf("\nAre you sure you want to add the following employee to the database?\n"); //prompt the user to confirm the information entered
    printf("\n%s %s %d\n\n", fname, lname, salary);
    printf("Enter Y for Yes; N for No: ");
    scanf("%s", choice);
    while (strcasecmp(choice, "Y") != 0 && strcasecmp(choice, "N") != 0) {
        printf("\nPlease enter Y or N to confirm your choice: ");
        scanf("%s", choice);
    }
    if (strcasecmp(choice, "Y") == 0) { //store the employee with given information into the database and write it to the text file
        strcpy(list[num_of_employee].first_name, fname);
        strcpy(list[num_of_employee].last_name, lname);
        list[num_of_employee].salary = salary;
        list[num_of_employee].employee_id = list[num_of_employee - 1].employee_id + 1;
        fprintf(filename, "\n%d %s %s %d", list[num_of_employee].employee_id, list[num_of_employee].first_name, list[num_of_employee].last_name, list[num_of_employee].salary);
        printf("\nEmployee successfully added!\n\n");
        num_of_employee += 1;
        fclose(filename);
    } else if (strcasecmp(choice, "N") == 0) { //if user chooses NO, go back to the main function
        printf("\n");
    }
}

int main(int arg_num, char* arg[]){ //main function to run the program
    int option = 0;
    FILE *infile, *outfile;
    if (arg_num == 1) { //open small.txt by default
        infile = fopen("small.txt", "r");
        outfile = fopen("small.txt", "a");
    }
    if (arg_num == 2) { //open the file with the name given
        infile = fopen(arg[1], "r");
        outfile = fopen(arg[1], "a");
    }
    while (option != 5) { //get user's option to run the program
        print_menu();
        scanf("%d", &option);
        int i = 0;
        int num_of_employee = list_num(list);
        while (!feof(infile)) { //read each line in the text file into the database
            fscanf(infile, "%d %s %s %d", &list[i].employee_id, list[i].first_name, list[i].last_name, &list[i].salary);
            num_of_employee++;
            i++;
        }
        if (option == 1) { //print the database using print_database()
            print_database(infile);
            option = 0;
        }
        if (option == 2) { //search the employee with given employee ID using search_by_id()
            int id;
            printf("\nPlease enter the 6 digit employee ID: ");
            scanf("%d", &id);
            search_by_id(list, 0, num_of_employee - 1, id);
            option = 0;
        }
        if (option == 3) { //search the employee with given last name using search_by_last_name();
            char x[Max_Char];
            printf("\nPlease enter the last name: ");
            scanf("%s", x);
            search_by_last_name(x);
            option = 0;
        }
        if (option == 4) { //add an employee using add_employee()
            add_employee(outfile);
            option = 0;
        }
        if (option < 0 || option > 5) { //prompt the user to enter a valid option
            printf("\nThe option entered is not valid. Please enter a number between 1 and 5.\n\n");
        }
    }
    if (option == 5) { // exit the program
        printf("\nThank you for using the employee database! Goodbye!\n");
        fclose(infile);
    }
    return 0;
}
