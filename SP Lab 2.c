#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define Max_Employee 1024
#define Max_Name 64
#define Max_Char 1000

static struct Employee { //construct a structure for employee to store information
    int employee_id;
    char first_name[Max_Name];
    char last_name[Max_Name];
    int salary;
} list[Max_Employee];

FILE *infile, *outfile1, *outfile2;

int count_list(struct Employee list[]) {
    int count = 0;
    for (int i = 0; list[i].employee_id != 0; i++) {
        count += 1;
    }
    return count;
}

void print_menu(){ //function to print out the main menu
    printf("Employee Database Menu:\n");
    printf("------------------------------\n");
    printf("1. Print the Database\n");
    printf("2. Look Up by Employee ID\n");
    printf("3. Look Up by Last Name (Only One)\n");
    printf("4. Add an Employee\n");
    printf("5. Exit\n");
    printf("6. Remove an Employee\n");
    printf("7. Update an Employee's Information\n");
    printf("8. Print N Employees with Highest Salaries\n");
    printf("9. Look Up by Last Name (All)\n");
    printf("------------------------------\n");
    printf("Enter your choice: ");
}

void print_database(FILE* filename){ //function to print the employee database
    int i = 0;
    printf("\nEmployee ID    First Name    Last Name      Salary\n");
    printf("-------------------------------------------------------\n");
    int num_of_employee = count_list(list);
    for (i; i < num_of_employee - 1; i++) { //sort the list by employee ID
        for (int j = 0; j < num_of_employee - i - 1; j++) {
            if (list[j].employee_id > list[j+1].employee_id) {
                struct Employee temp;
                temp = list[j];
                list[j] = list[j+1];
                list[j+1] = temp;
            }
        }
    }
    i = 0;
    while (list[i].employee_id != 0) { //print out the employee database
        printf("  %-13d %-13s %-14s%6d\n", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
        i++;
    }
    printf("-------------------------------------------------------\n");
    printf("Number of Employees: %d\n\n", num_of_employee); //print out the number of employees in the database
}

void search_by_id(struct Employee list[], int l, int r, int x) { //function to search by ID using binary search
    int found = 0;
    while (l <= r) {
        int mid = l + (r - l) / 2;
        if (list[mid].employee_id == x) {
            printf("\nEmployee ID    First Name    Last Name      Salary\n");
            printf("-------------------------------------------------------\n");
            printf("  %-13d %-13s %-14s%6d\n", list[mid].employee_id, list[mid].first_name, list[mid].last_name, list[mid].salary);
            printf("-------------------------------------------------------\n\n");
            found = 1;
            break;
        }
        if (list[mid].employee_id < x) {
            l = mid + 1;
            found = 0;
        }
        else {
            r = mid - 1;
            found = 0;
        }
    }
    if (found == 0) {
        printf("\nEmployee with the ID %d cannot be found in the database.\n\n", x);
    }
}

void search_by_last_name_one(char x[Max_Char]) { //function to search by last name
    int found = 0;
    int match = 0;
    struct Employee match_list[Max_Employee]; //create a new list to store the employees who match the search criteria
    for (int i = 0; i < Max_Employee; i++) {
        if (strcasecmp(x, list[i].last_name) == 0) { //check whether two strings are case-insensitively equal
            match_list[match] = list[i];
            match++;
            found = 1;
            break;
        } else if (found != 1) {
            found = 0;
        }
    }
    if (match == 0) { //print out the "not found" message
        printf("\nEmployee with the last name %s is not found in the database.\n\n", x);
    } else { //print out the employees with given last name
        printf("\nEmployee ID    First Name    Last Name      Salary\n");
        printf("-------------------------------------------------------\n");
        for (int j = 0; j < match; j++) {
            printf("  %-13d %-13s %-14s%6d\n", match_list[j].employee_id, match_list[j].first_name, match_list[j].last_name, match_list[j].salary);
        }
        printf("-------------------------------------------------------\n\n");
    }
}

void add_employee(FILE* filename) { //function to add an employee
    char fname[Max_Name], lname[Max_Name], annual_salary[Max_Char], choice[Max_Char]; //define three strings to read user's input
    int i = 0, check = 0;
    int num_of_employee = count_list(list);
    printf("\nPlease enter the first name of the employee: ");
    getchar();
    scanf("%[^\n]", fname);
    do { // check whether the field contains extra spaces
        i = 0;
        check = 0;
        while(fname[i] != '\0') {
            if (isalpha(fname[i]) == 0) {
                check = 1;
                printf("\nPlease only enter the name with no extra spaces or middle initials. Please re-enter: ");
                getchar();
                scanf("%[^\n]", fname);
                break;
            }
            i++;
        }
    } while (check == 1);
    i = 0;
    check = 0;
    printf("\nPlease enter the last name of the employee: ");
    getchar();
    scanf("%[^\n]", lname);
    do { //check whether the field contains extra spaces
        i = 0;
        check = 0;
        while(lname[i] != '\0') {
            if (isalpha(lname[i]) == 0) {
                check = 1;
                printf("\nPlease only enter the name with no extra spaces or middle initials. Please re-enter: ");
                getchar();
                scanf("%[^\n]", lname);
                break;
            }
            i++;
        }
    } while (check == 1);
    printf("\nEnter the salary for the employee (30000 to 150000): ");
    scanf("%s", annual_salary);
    int a = 0;
    while (isalpha(annual_salary[a])) { //check whether user's input contains non-digit character
        printf("\nPlease enter an integer between 30000 and 150000. Please re-enter: ");
        scanf("%s", annual_salary);
        a++;
        while (atoi(annual_salary) < 30000 || atoi(annual_salary) > 150000) {
            printf("\nPlease enter an integer between 30000 and 150000. Please re-enter: ");
            scanf("%s", annual_salary);
        }
    }
    int salary = atoi(annual_salary);
    printf("\nAre you sure you want to add the following employee to the database?\n"); //prompt the user to confirm the information entered
    printf("\n%s %s %d\n\n", fname, lname, salary);
    printf("Enter Y for Yes; N for No: ");
    scanf("%s", choice);
    while (strcasecmp(choice, "Y") != 0 && strcasecmp(choice, "N") != 0) {
        printf("\nPlease enter Y or N to confirm your choice: ");
        scanf("%s", choice);
    }
    if (strcasecmp(choice, "Y") == 0) { //store the employee with given information into the database and write it to the text file
        strcpy(list[num_of_employee].first_name, fname);
        strcpy(list[num_of_employee].last_name, lname);
        list[num_of_employee].salary = salary;
        list[num_of_employee].employee_id = list[num_of_employee - 1].employee_id + 1;
        fprintf(filename, "\n%d %s %s %d", list[num_of_employee].employee_id, list[num_of_employee].first_name, list[num_of_employee].last_name, list[num_of_employee].salary);
        printf("\nEmployee successfully added!\n\n");
        fclose(filename);
    } else if (strcasecmp(choice, "N") == 0) { //if user chooses NO, go back to the main function
        printf("\n");
    }
}

void remove_employee(char x[Max_Char], FILE* filename) { //function to remove an employee
    char choice[Max_Char];
    int num_of_employee = count_list(list);
    int match = 0, found = 0, a = 0;
    while (isalpha(x[a])) { //check whether user's input contains non-digit character
        printf("\nPlease enter an integer between 100000 and 999999. Please re-enter: ");
        scanf("%s", x);
        a++;
        while (atoi(x) < 100000 || atoi(x) > 999999) {
            printf("\nPlease enter an integer between 100000 and 999999. Please re-enter: ");
            scanf("%s", x);
        }
    }
    int id = atoi(x);
    for (int i = 0; i < num_of_employee; i++) { //find if such employee with the given ID exists
        if (id == list[i].employee_id) {
            found = 1;
            break;
        } else if (found != 1) {
            found = 0;
        }
        match++;
    }
    if (found == 0) { //print out the "not found" message and print the list back to the text file
        printf("\nEmployee with the ID %d cannot be found in the database.\n\n", id);
        fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
        for (int i = 1; i < num_of_employee; i++) {
            fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
        }
        fclose(filename);
    } else if (found == 1) {
        printf("\nAre you sure you want to remove this employee?\n");
        printf("\nEmployee ID    First Name    Last Name      Salary\n");
        printf("-------------------------------------------------------\n");
        printf("  %-13d %-13s %-14s%6d\n", list[match].employee_id, list[match].first_name, list[match].last_name, list[match].salary);
        printf("-------------------------------------------------------\n");
        printf("\nEnter Y for Yes; N for No: ");
        scanf("%s", choice);
        while (strcasecmp(choice, "Y") != 0 && strcasecmp(choice, "N") != 0) {
            printf("\nPlease enter Y or N to confirm your choice: ");
            scanf("%s", choice);
        }
        if (strcasecmp(choice, "Y") == 0) {
            for (int i = match; i < num_of_employee; i++) { //remove the line with the given employee ID by replacing each line with the line after it
                list[i] = list[i + 1];
            }
            list[num_of_employee].employee_id = '\0'; //set the last line to null
            list[num_of_employee].first_name[0] = '\0';
            list[num_of_employee].last_name[0] = '\0';
            list[num_of_employee].salary = '\0';
            fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary); //print the first line of the result back to the text file
            for (int i = 1; i < num_of_employee - 1; i++) { //print other lines back to the text file
                fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
            }
            printf("\nEmployee successfully removed!\n\n");
            fclose(filename);
        } else if (strcasecmp(choice, "N") == 0) { //if user chooses NO, go back to the main function and reprint the list to the text file, otherwise, the text file would be empty
            fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
            for (int i = 1; i < num_of_employee; i++) {
                fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
            }
            fclose(filename);
            printf("\n");
        }
    }
}

void update_info(char x[Max_Char], FILE* filename) { //function to update the information of an employee
    char choice[Max_Char];
    int option = atoi(choice);
    int num_of_employee = count_list(list);
    int match = 0, found = 0, a = 0;
    while (isalpha(x[a])) { //check whether user's input contains non-digit character
        printf("\nPlease enter an integer between 100000 and 999999. Please re-enter: ");
        scanf("%s", x);
        a++;
        while (atoi(x) < 100000 || atoi(x) > 999999) {
            printf("\nPlease enter an integer between 100000 and 999999. Please re-enter: ");
            scanf("%s", x);
        }
    }
    int id = atoi(x);
    for (int i = 0; i < num_of_employee; i++) { //check if such employee with the given ID exists
        if (id == list[i].employee_id) {
            found = 1;
            break;
        } else if (found != 1) {
            found = 0;
        }
        match++;
    }
    if (found == 0) { //print out the "not found" message and print the list back to the text file
        printf("\nEmployee with the ID %d cannot be found in the database.\n\n", id);
        fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
        for (int i = 1; i < num_of_employee; i++) {
            fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
        }
        fclose(filename);
    } else if (found == 1) {
        printf("\nEmployee ID    First Name    Last Name      Salary\n");
        printf("-------------------------------------------------------\n");
        printf("  %-13d %-13s %-14s%6d\n", list[match].employee_id, list[match].first_name, list[match].last_name, list[match].salary);
        printf("-------------------------------------------------------\n");
        printf("Which field would you like to update for this employee?\n"); //prompt the user to choose from the options below
        printf("\n1. Employee ID\n");
        printf("2. First Name\n");
        printf("3. Last Name\n");
        printf("4. Salary\n");
        printf("5. Back to the Main Menu\n");
        printf("\nEnter your choice: ");
        scanf("%s", choice);
        int a = 0;
        while (isalpha(choice[a]) || atoi(choice) < 0 || atoi(choice) > 5) { //make sure the user enters an integer number between 1 and 5
            printf("\nPlease enter an integer between 1 and 5. Please re-enter: ");
            scanf("%s", choice);
            a++;
        }
        option = atoi(choice);
        if (option == 1) { //update the employee ID of that specific employee
            char x[Max_Char], choice1[Max_Char];
            printf("\nPlease enter the new 6 digit ID for this employee: ");
            scanf("%s", x);
            while (isalpha(x[a])) { //check whether user's input contains non-digit character
                printf("\nPlease enter an integer between 100000 and 999999. Please re-enter: ");
                scanf("%s", x);
                a++;
                while (atoi(x) < 100000 || atoi(x) > 999999) {
                    printf("\nPlease enter an integer between 100000 and 999999. Please re-enter: ");
                    scanf("%s", x);
                }
            }
            int new_id = atoi(x);
            printf("\nAre you sure you want to change the employee ID like this?\n"); //prompt the user to check whether he/she wants to change the information
            printf("\nEmployee ID    First Name    Last Name      Salary\n");
            printf("-------------------------------------------------------\n");
            printf("  %-13d %-13s %-14s%6d\n", new_id, list[match].first_name, list[match].last_name, list[match].salary);
            printf("-------------------------------------------------------\n");
            printf("\nEnter Y for Yes; N for No: ");
            scanf("%s", choice1);
            while (strcasecmp(choice1, "Y") != 0 && strcasecmp(choice1, "N") != 0) { //check the user enters a valid option
                printf("\nPlease enter Y or N to confirm your choice: ");
                scanf("%s", choice1);
            }
            if (strcasecmp(choice1, "Y") == 0) { //update the old employee ID with the new ID
                list[match].employee_id = new_id;
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) { //write the list back to the text file with updated information
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                printf("\nEmployee successfully updated!\n\n");
                fclose(filename);
            } else if (strcasecmp(choice1, "N") == 0) { //if user chooses NO, go back to the main function and reprint the list to the text file, otherwise, the text file would be empty
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) {
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                fclose(filename);
                printf("\n");
            }
            option = 0;
        }
        if (option == 2) { //update the first name of that specific employee
            char new_fname[Max_Char];
            char choice2[Max_Char];
            int i = 0, check = 0;
            printf("\nPlease enter the new first name for this employee: ");
            getchar();
            scanf("%[^\n]", new_fname);
            do { // check whether the field contains extra spaces
                i = 0;
                check = 0;
                while(new_fname[i] != '\0') {
                    if (isalpha(new_fname[i]) == 0) {
                        check = 1;
                        printf("\nPlease only enter the name with no extra spaces or middle initials. Please re-enter: ");
                        getchar();
                        scanf("%[^\n]", new_fname);
                        break;
                    }
                    i++;
                }
            } while (check == 1);
            printf("\nAre you sure you want to change the employee's first name like this?\n");
            printf("\nEmployee ID    First Name    Last Name      Salary\n");
            printf("-------------------------------------------------------\n");
            printf("  %-13d %-13s %-14s%6d\n", list[match].employee_id, new_fname, list[match].last_name, list[match].salary);
            printf("-------------------------------------------------------\n");
            printf("\nEnter Y for Yes; N for No: "); //prompt the user if he/she wants to make the change
            scanf("%s", choice2);
            while (strcasecmp(choice2, "Y") != 0 && strcasecmp(choice2, "N") != 0) { //check whether the user enters a valid option
                printf("\nPlease enter Y or N to confirm your choice: ");
                scanf("%s", choice2);
            }
            if (strcasecmp(choice2, "Y") == 0) { //replace the old first name with the new one
                strcpy(list[match].first_name, new_fname);
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) { //write the list back to the text file with updated information
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                printf("\nEmployee successfully updated!\n\n");
                fclose(filename);
            } else if (strcasecmp(choice, "N") == 0) { //if user chooses NO, go back to the main function and reprint the list to the text file, otherwise, the text file would be empty
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) {
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                fclose(filename);
                printf("\n");
            }
            option = 0;
        }
        if (option == 3) { //update the last name of that specific employee
            char new_lname[Max_Char];
            char choice3[Max_Char];
            int i = 0, check = 0;
            printf("\nPlease enter the new last name for this employee: ");
            getchar();
            scanf("%[^\n]", new_lname);
            do { // check whether the field contains extra spaces
                i = 0;
                check = 0;
                while(new_lname[i] != '\0') {
                    if (isalpha(new_lname[i]) == 0) {
                        check = 1;
                        printf("\nPlease only enter the name with no extra spaces or middle initials. Please re-enter: ");
                        getchar();
                        scanf("%[^\n]", new_lname);
                        break;
                    }
                    i++;
                }
            } while (check == 1);
            printf("\nAre you sure you want to change the employee's last name like this?\n");
            printf("\nEmployee ID    First Name    Last Name      Salary\n");
            printf("-------------------------------------------------------\n");
            printf("  %-13d %-13s %-14s%6d\n", list[match].employee_id, list[match].first_name, new_lname, list[match].salary);
            printf("-------------------------------------------------------\n");
            printf("\nEnter Y for Yes; N for No: "); //prompt the user whether he/she wants to make the change
            scanf("%s", choice3);
            while (strcasecmp(choice3, "Y") != 0 && strcasecmp(choice3, "N") != 0) { //check whether the user enters a valid option
                printf("\nPlease enter Y or N to confirm your choice: ");
                scanf("%s", choice3);
            }
            if (strcasecmp(choice3, "Y") == 0) { //replace the old last name with the new one
                strcpy(list[match].last_name, new_lname);
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) { //write the list back to the text file with updated information
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                printf("\nEmployee successfully updated!\n\n");
                fclose(filename);
            } else if (strcasecmp(choice, "N") == 0) { //if user chooses NO, go back to the main function and reprint the list to the text file, otherwise, the text file would be empty
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) {
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                fclose(filename);
                printf("\n");
            }
            option = 0;
        }
        if (option == 4) { //update the salary of that specific employee
            char choice4[Max_Char], x[Max_Char];
            printf("\nPlease enter the new salary for this employee: ");
            scanf("%s", x);
            while (isalpha(x[a])) { //check whether user's input contains non-digit character
                printf("\nPlease enter an integer between 30000 and 150000. Please re-enter: ");
                scanf("%s", x);
                a++;
                while (atoi(x) < 30000 || atoi(x) > 150000) {
                    printf("\nPlease enter an integer between 30000 and 150000. Please re-enter: ");
                    scanf("%s", x);
                }
            }
            int new_salary = atoi(x);
            printf("\nAre you sure you want to change the employee's salary like this?\n");
            printf("\nEmployee ID    First Name    Last Name      Salary\n");
            printf("-------------------------------------------------------\n");
            printf("  %-13d %-13s %-14s%6d\n", list[match].employee_id, list[match].first_name, list[match].last_name, new_salary);
            printf("-------------------------------------------------------\n");
            printf("\nEnter Y for Yes; N for No: "); //prompt the user whether he/she wants to make the change
            scanf("%s", choice4);
            while (strcasecmp(choice4, "Y") != 0 && strcasecmp(choice4, "N") != 0) { //check whether the user enters a valid option
                printf("\nPlease enter Y or N to confirm your choice: ");
                scanf("%s", choice4);
            }
            if (strcasecmp(choice4, "Y") == 0) { //replace the old salary with the new salary
                list[match].salary = new_salary;
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) { //write the list back to the text file with updated information
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                printf("\nEmployee successfully updated!\n\n");
                fclose(filename);
            } else if (strcasecmp(choice4, "N") == 0) { //if user chooses NO, go back to the main function and reprint the list to the text file, otherwise, the text file would be empty
                fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
                for (int i = 1; i < num_of_employee; i++) {
                    fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
                }
                fclose(filename);
                printf("\n");
            }
            option = 0;
        }
    }
    if (option == 5) { //print the list back to the text file if user decides not to change anything
        fprintf(filename, "%d %s %s %d", list[0].employee_id, list[0].first_name, list[0].last_name, list[0].salary);
        for (int i = 1; i < num_of_employee; i++) {
            fprintf(filename, "\n%d %s %s %d", list[i].employee_id, list[i].first_name, list[i].last_name, list[i].salary);
        }
        fclose(filename);
        printf("\n");
    }
}

void highest_salaries(int n) { //function to return n employee with the highest salaries
    int i = 0;
    int num_of_employee = count_list(list);
    for (i; i < num_of_employee - 1; i++) { //sort the list by descending salaries
        for (int j = 0; j < num_of_employee - i - 1; j++) {
            if (list[j].salary < list[j+1].salary) {
                struct Employee temp;
                temp = list[j];
                list[j] = list[j+1];
                list[j+1] = temp;
            }
        }
    }
    printf("\nEmployee ID    First Name    Last Name      Salary\n");
    printf("-------------------------------------------------------\n");
    for (int a = 0; a < n; a++) { //print out the first n employees with the highest salaries
        printf("  %-13d %-13s %-14s%6d\n", list[a].employee_id, list[a].first_name, list[a].last_name, list[a].salary);
    }
    printf("-------------------------------------------------------\n\n");
    i = 0;
    for (i; i < num_of_employee - 1; i++) { //resort the list by ID again
        for (int j = 0; j < num_of_employee - i - 1; j++) {
            if (list[j].employee_id > list[j+1].employee_id){
                struct Employee temp;
                temp = list[j];
                list[j] = list[j+1];
                list[j+1] = temp;
            }
        }
    }
}

void search_by_last_name_all(char x[Max_Char]) { //function to search by last name
    int found = 0;
    int match = 0;
    struct Employee match_list[Max_Employee]; //create a new list to store the employees who match the search criteria
    for (int i = 0; i < Max_Employee; i++) {
        if (strcasecmp(x, list[i].last_name) == 0) { //check whether two strings are case-insensitively equal
            match_list[match] = list[i];
            match++;
            found = 1;
        } else if (found != 1) {
            found = 0;
        }
    }
    if (match == 0) { //print out the "not found" message
        printf("\nEmployee with the last name %s is not found in the database.\n\n", x);
    } else { //print out the employees with given last name
        printf("\nEmployee ID    First Name    Last Name      Salary\n");
        printf("-------------------------------------------------------\n");
        for (int j = 0; j < match; j++) {
            printf("  %-13d %-13s %-14s%6d\n", match_list[j].employee_id, match_list[j].first_name, match_list[j].last_name, match_list[j].salary);
        }
        printf("-------------------------------------------------------\n\n");
    }
}

int main(int arg_num, char* arg[]){ //main function to run the program
    char in[Max_Char];
    int option = atoi(in);
    while (option != 5) { //get user's option to run the program
        print_menu();
        scanf("%s", in);
        int a = 0;
        while (isalpha(in[a])) { //check whether user's option contains non-digit character
            printf("\nPlease enter an integer number between 1 and 9. Please re-enter: ");
            scanf("%s", in);
            a++;
        }
        option = atoi(in);
        int i = 0;
        int num_of_employee = count_list(list);
        if (arg_num == 1) { //open small.txt in "read" mode by default
            infile = fopen("small.txt", "r");
        }
        if (arg_num == 2) { //open the file with the name given in "read" mode
            infile = fopen(arg[1], "r");
        }
        while (!feof(infile)) { //read each line in the text file into the database
            fscanf(infile, "%d %s %s %d", &list[i].employee_id, list[i].first_name, list[i].last_name, &list[i].salary);
            i++;
        }
        if (option == 1) { //print the database using print_database()
            print_database(infile);
            option = 0;
        }
        if (option == 2) { //search the employee with given employee ID using search_by_id()
            int id;
            printf("\nPlease enter the 6 digit employee ID: ");
            scanf("%d", &id);
            search_by_id(list, 0, num_of_employee - 1, id);
            option = 0;
        }
        if (option == 3) { //print out only one employee with given last name using search_by_last_name()
            char x[Max_Char];
            printf("\nPlease enter the last name: ");
            scanf("%s", x);
            search_by_last_name_one(x);
            option = 0;
        }
        if (option == 4) { //add an employee using add_employee()
            if (arg_num == 1) { //open small.txt in "append" mode by default
                outfile1 = fopen("small.txt", "a");
            }
            if (arg_num == 2) { //open the file with the name given in "append" mode
                outfile1 = fopen(arg[1], "a");
            }
            add_employee(outfile1);
            option = 0;
        }
        if (option == 6) { //remove an employee using remove_employee()
            if (arg_num == 1) { //open small.txt in "write" mode by default
                outfile2 = fopen("small.txt", "w");
            }
            if (arg_num == 2) { //open the file with the name given in "write" mode
                outfile2 = fopen(arg[1], "w");
            }
            char x[Max_Char];
            printf("\nPlease enter the 6 digit employee ID of the employee you want to remove: ");
            scanf("%s", x);
            remove_employee(x, outfile2);
            option = 0;
        }
        if (option == 7) { //update the information of an employee using update_info()
            if (arg_num == 1) { //open small.txt in "write" mode by default
                outfile2 = fopen("small.txt", "w");
            }
            if (arg_num == 2) { //open the file with the name given in "write" mode
                outfile2 = fopen(arg[1], "w");
            }
            char id[Max_Char];
            printf("\nPlease enter the 6 digit employee ID of the employee you want to update: ");
            scanf("%s", id);
            update_info(id, outfile2);
            option = 0;
        }
        if (option == 8) { //print out n employees with the highest salaries using highest_salaries()
            int n;
            printf("\nPlease enter how many employees you want to return with the highest salaries: ");
            scanf("%d", &n);
            highest_salaries(n);
            option = 0;
        }
        if (option == 9) { //print out all employees with given last name using search_by_last_name()
            char x[Max_Char];
            printf("\nPlease enter the last name: ");
            scanf("%s", x);
            search_by_last_name_all(x);
            option = 0;
        }
        if (option < 0 || option > 9) { //prompt the user to enter a valid number between 1 and 9
            printf("\nThe option entered is not valid. Please enter a number between 1 and 9.\n\n");
        }
    }
    if (option == 5) { // exit the program
        printf("\nThank you for using the employee database! Goodbye!\n");
        fclose(infile);
    }
    return 0;
}
