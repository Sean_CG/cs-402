#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double average(float arr[], int count) { //function to calculate the average
    double sum = 0;
    for (int i = 0; i < count; i++) {
        sum += (double)arr[i];
    }
    return sum / (double)count;
}

float median(float arr[], int count) { //function to find the median
    for (int i = 0; i < count - 1; i++) { //sort the list in ascending order using bubble sort
        for (int j = 0; j < count - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                float temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    if (count % 2 == 0) {
        return (arr[count / 2] + arr[(count / 2) - 1]) / 2.0;
    }
    if (count % 2 == 1) {
        return arr[count / 2];
    }
}

double std_dev(float arr[], int count) { //function to calculate the standard deviation
    double mean = average(arr, count);
    float difference, variance;
    float var_sum = 0;
    for (int i = 0; i < count; i++) { //calculate variance first
        difference = arr[i] - (float)mean;
        var_sum += pow(difference, 2);
    }
    variance = var_sum / (float)count; //then divide the variance by the number of elements
    double stddev = sqrt(variance);
    return stddev;
}

void resize(float **original, int size) { //function to expand the current array
    size *= 2;
    float *resized = (float*)malloc(size * sizeof(float));; //create a new dynamically allocated array
    for (int i = 0; i < size; i++) { //copy the values from the current array to the new one
        resized[i] = (*original)[i];
    }
    free(*original); //free the memory of the current array
    *original = resized; //set the new array to be the current array
}

int main(int argc, char* argv[]) { //main function to run the program
    FILE *infile;
    if (argc == 1) { //exit the program if no file name is given
        printf("Failed when opening the file!\n");
        exit(0);
    }
    if (argc == 2) { //open the file with the name given in read mode
        infile = fopen(argv[1], "r");
    }
    int num_of_element = 0, i = 0;
    float *arr;
    int n = 20;
    arr = (float*)malloc(n * sizeof(float)); //create a dynamically allocated array of floats
    while (!feof(infile)){ //read the input from the file
        fscanf(infile, "%f", &arr[i]);
        i++;
        num_of_element++;
        while (i > n) { //expand the current array using resize()
            resize(&arr, n);
            n *= 2;
        }
    }
    double mean = average(arr, num_of_element); //calculate the average of an array using average()
    float med = median(arr, num_of_element); //calculate the median of an array using median()
    double stddev = std_dev(arr, num_of_element); //calculate the standard deviation of an array using std_dev()
    printf("Results:\n");
    printf("------------------------------\n");
    printf("  Number of Values:    %d\n", num_of_element);
    printf("       Mean:         %.3lf\n", mean);
    printf("      Median:        %.3f\n", med);
    printf(" Standard Deviation: %.3lf\n", stddev);
    printf("Unused Array Capacity: %d\n", (n - num_of_element));
    fclose(infile); //close the file
    free(arr); //free the memory occupied by the array
    return 0;
}
